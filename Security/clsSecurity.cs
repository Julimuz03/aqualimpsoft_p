﻿using ConexionDB;
using SimpleCrypto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security
{
    public class clsSecurity
    {

        public DataTable CrearUsuario(string IdEmpleado, string Usuario, string Pass, int IdRango, string UsuarioLog)
        {

            try {
                clsDatos objConsulta = new clsDatos();

                ICryptoService CryptoService = new PBKDF2();

                string SaltKey = CryptoService.GenerateSalt();

                string PassEncrypt = CryptoService.Compute(Pass, SaltKey);

                DataTable dtResul = objConsulta.CrearUsuario(IdEmpleado, Usuario, PassEncrypt, SaltKey, IdRango, UsuarioLog);


                return dtResul;
            }
            catch(Exception e)
            {
                DataTable dtResul = new DataTable(); 

                DataRow drCelda = dtResul.NewRow();

                drCelda["ErrorMessage"] = e.Message.ToString();

                dtResul.Rows.Add(drCelda);

                return dtResul;
            }

        }

        public (bool Bool, DataTable Tabla) ValidarUsuario(string Usuario, string Pass)
        {
            try
            {
                clsDatos objConsulta = new clsDatos();

                ICryptoService CryptoService = new PBKDF2();

                DataTable dtSaltKey = objConsulta.ValidarUsuarios(Usuario);

                string PassEncrypt = CryptoService.Compute(Pass, dtSaltKey.Rows[0][1].ToString());

                bool IsValidPassword = CryptoService.Compare(dtSaltKey.Rows[0][0].ToString(), PassEncrypt);

                if (IsValidPassword)
                {
                    return (true, dtSaltKey);
                }
                else
                {
                    return (false, dtSaltKey);
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }

}
