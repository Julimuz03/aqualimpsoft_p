﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionDB;

namespace Security.Negocio
{
    public class clsNegocio
    {
        clsDatos objConsul = new clsDatos();

        public DataTable LoginUsuarios(string Usuario)
        {
            DataTable dtInfo = objConsul.LoginUsuarios(Usuario);

            return dtInfo;
        }

        public DataTable CBORangos()
        {
            DataTable dtInfo = objConsul.CBORangos();

            return dtInfo;
        }

        public DataTable ValidarNomEmpleado(string IdEmpleado)
        {
            DataTable dtInfo = objConsul.ValidarNomEmpleado(IdEmpleado);

            return dtInfo;
        }

        public DataSet getServiciosTecnico(string IdEmpleado)
        {
            DataSet dtInfo = objConsul.getServiciosTecnico(IdEmpleado);

            return dtInfo;
        }

        public DataTable getNotasServicio(int idServicio)
        {
            DataTable dtInfo = objConsul.getNotasServicio(idServicio);

            return dtInfo;
        }

        public DataTable setServicioRealizado(int idServicio, string usuarioLog, string albaranArchivo, string notaTecnico, string firmaCliente)
        {
            DataTable dtInfo = objConsul.setServicioRealizado(idServicio, usuarioLog, albaranArchivo, notaTecnico, firmaCliente);

            return dtInfo;
        }

        public DataTable getServiciosPteRevision()
        {
            DataTable dtInfo = objConsul.getServiciosPteRevision();

            return dtInfo;
        }

        public DataTable setCrearServicio(int idCliente,
                                          string nombreCliente,
                                          string apellidoCliente,
                                          string direccionServicio,
                                          string telefonoCliente,
                                          string emailCliente,
                                          int idPoblacion,
                                          int tipoServicio,
                                          string idTecnico,
                                          int idPasadoX,
                                          int idEstado,
                                          int idLocalizacion,
                                          string usuarioCreacion,
                                          string descripcionServicio,
                                          string notaAgenda,
                                          string fechaAgenda,
                                          string fechaCreacion,
                                          string usuarioLog)
        {
            DataTable dtInfo = objConsul.setCrearServicio(idCliente,
                                                          nombreCliente,
                                                          apellidoCliente,
                                                          direccionServicio,
                                                          telefonoCliente,
                                                          emailCliente,
                                                          idPoblacion,
                                                          tipoServicio,
                                                          idTecnico,
                                                          idPasadoX,
                                                          idEstado,
                                                          idLocalizacion,
                                                          usuarioCreacion,
                                                          descripcionServicio,
                                                          notaAgenda,
                                                          fechaAgenda,
                                                          fechaCreacion,
                                                          usuarioLog);

            return dtInfo;
        }

        public DataSet getInfoPpalServicios()
        {
            DataSet dtInfo = objConsul.getInfoPpalServicios();

            return dtInfo;
        }

        public DataSet getPoblaciones(int idProvincia, int idTipoServicio)
        {
            DataSet dtInfo = objConsul.getPoblaciones(idProvincia, idTipoServicio);

            return dtInfo;
        }

        public DataTable Formula(int idFormula)
        {
            DataTable dtInfo = objConsul.Formula(idFormula);

            return dtInfo;
        }

        //public DataTable setServicioRealizado()
        //{
        //   // DataTable dtInfo = objConsul.Formula();

        //    return dtInfo;
        //}
    }

}
