﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionDB
{
    public class clsDatos
    {
        ConexionDB AquaSoftConexion = new ConexionDB();
        public DataTable LoginUsuarios(string usuario)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spLoginUsuarios]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@USUARIO", usuario);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable CrearUsuario(string IdEmpleado, string usuario, string pass, string SaltKey, int IdRango, string UsuarioLog)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spCrearUsuario]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@IdEmpleado", IdEmpleado);
            datosprocedimiento.Parameters.AddWithValue("@Usuario", usuario);
            datosprocedimiento.Parameters.AddWithValue("@Pass", pass);
            datosprocedimiento.Parameters.AddWithValue("@SaltKey", SaltKey);
            datosprocedimiento.Parameters.AddWithValue("@IdRango", IdRango);
            datosprocedimiento.Parameters.AddWithValue("@UsuarioLog", UsuarioLog);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable ValidarUsuarios(string usuario)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spValidarUsuario]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@Usuario", usuario);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable CBORangos()
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spCBORango]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable ValidarNomEmpleado(string IdEmpleado)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spValidarNomEmpleado]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@Id_Empleado", IdEmpleado);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataSet getServiciosTecnico(string IdEmpleado)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spGetServiciosTecnicos]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataSet datainfo = new DataSet();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@ID_EMPLEADO", IdEmpleado);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable getNotasServicio(int idServicio)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spGetNotasServicio]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@ID_SERVICIO", idServicio);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable setServicioRealizado(int idServicio, string usuarioLog, string albaranArchivo, string notaTecnico, string firmaCliente)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spServicioRealizado]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@ID_SERVICIO", idServicio);
            datosprocedimiento.Parameters.AddWithValue("@USUARIO_LOG", usuarioLog);
            datosprocedimiento.Parameters.AddWithValue("@ALBARAN_ARCHIVO", albaranArchivo);
            datosprocedimiento.Parameters.AddWithValue("@NOTA_TECNICO", notaTecnico);
            datosprocedimiento.Parameters.AddWithValue("@FIRMA_CLIENTE", firmaCliente);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable getServiciosPteRevision()
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spServiciosPteRevision]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable setCrearServicio(int idCliente,
                                          string nombreCliente,
                                          string apellidoCliente,
                                          string direccionServicio,
                                          string telefonoCliente,
                                          string emailCliente,
                                          int idPoblacion,
                                          int tipoServicio,
                                          string idTecnico,
                                          int idPasadoX,
                                          int idEstado,
                                          int idLocalizacion,
                                          string usuarioCreacion,
                                          string descripcionServicio,
                                          string notaAgenda,
                                          string fechaAgenda,
                                          string fechaCreacion,
                                          string usuarioLog)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spCrearServicio]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@ID_CLIENTE", idCliente);
            datosprocedimiento.Parameters.AddWithValue("@NOMBRE_CLIENTE", nombreCliente);
            datosprocedimiento.Parameters.AddWithValue("@APELLIDO_CLIENTE", apellidoCliente);
            datosprocedimiento.Parameters.AddWithValue("@DIRECCION_SERVICIO", direccionServicio);
            datosprocedimiento.Parameters.AddWithValue("@TELEFONO_SERVICIO", telefonoCliente);
            datosprocedimiento.Parameters.AddWithValue("@EMAIL_CLIENTE", emailCliente);
            datosprocedimiento.Parameters.AddWithValue("@ID_POBLACION", idPoblacion);
            datosprocedimiento.Parameters.AddWithValue("@ID_TIPO_SERVICIO", tipoServicio);
            datosprocedimiento.Parameters.AddWithValue("@ID_TECNICO", idTecnico);
            datosprocedimiento.Parameters.AddWithValue("@ID_PASADOX", idPasadoX);
            datosprocedimiento.Parameters.AddWithValue("@ID_ESTADO", idEstado);
            datosprocedimiento.Parameters.AddWithValue("@ID_LOCALIZACION", idLocalizacion);
            datosprocedimiento.Parameters.AddWithValue("@USUARIO_CREACION", usuarioCreacion);
            datosprocedimiento.Parameters.AddWithValue("@DESC_SERVICIO", descripcionServicio);
            datosprocedimiento.Parameters.AddWithValue("@PLANIFICACION_AGENDA", notaAgenda);
            datosprocedimiento.Parameters.AddWithValue("@FECHA_PLANIFICACION", fechaAgenda);
            datosprocedimiento.Parameters.AddWithValue("@FECHA_CREACION", fechaCreacion);
            datosprocedimiento.Parameters.AddWithValue("@USUARIO_LOG", usuarioLog);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataSet getInfoPpalServicios()
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spGetInfoPpalServicios]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataSet datainfo = new DataSet();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataSet getPoblaciones(int idProvincia, int idTipoServicio)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spGetPoblaciones]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataSet datainfo = new DataSet();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@ID_PROVINCIA", idProvincia);
            datosprocedimiento.Parameters.AddWithValue("@ID_TIPO_SERVICIO", idTipoServicio);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable Formula(int idFormula)
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spFormula]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            datosprocedimiento.Parameters.AddWithValue("@ID_PROVINCIA", idFormula);

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

        public DataTable setServicioRealizado()
        {
            SqlCommand datosprocedimiento = new SqlCommand("[dbo].[spFormula]", AquaSoftConexion.ConexionSQL);
            SqlDataAdapter adapterdatos = new SqlDataAdapter();
            DataTable datainfo = new DataTable();
            datosprocedimiento.CommandType = CommandType.StoredProcedure;

            adapterdatos.SelectCommand = datosprocedimiento;

            AquaSoftConexion.AbreConexion();
            adapterdatos.Fill(datainfo);
            AquaSoftConexion.CierraConexion();

            return datainfo;

        }

    }

}
