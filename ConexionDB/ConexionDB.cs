﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;

namespace ConexionDB
{
    public class ConexionDB
    {
        private SqlConnection obconexion;


        public ConexionDB()
        {
            //string sconecta = "Data Source=AQUA-EQ-JMC\\SQLExpress;Initial Catalog=BD_AQUALIMP;Integrated Security=True";
            string sconecta = ConfigurationManager.ConnectionStrings["ConexPrincipal"].ToString();
            //string sconecta = "Data Source = sql6006.site4now.net; Initial Catalog = DB_A4D01F_praqualimp; Persist Security Info = True; User ID = DB_A4D01F_praqualimp_admin; Password = Aqua.limp24.h";

            obconexion = new SqlConnection(sconecta);
        }


        public SqlConnection ConexionSQL
        {
            get { return obconexion; }
        }


        public void AbreConexion()
        {

            if (obconexion.State.Equals(ConnectionState.Closed))
            {
                obconexion.Open();
            }

        }


        public void CierraConexion()
        {

            if (!obconexion.State.Equals(ConnectionState.Closed))
            {

                obconexion.Close();
            }
        }
    }
}
