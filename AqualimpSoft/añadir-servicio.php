<html>
<?php
  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );

  include_once('session/sessions.php');
?>
  <head>
    <?php include_once('includes/head.php');?>
    <link href="assets/datepicker/css/bootstrap-datepicker.standalone.css" rel="stylesheet">
  </head>
  <body>
    <div id="main">
    <?php
      include_once('includes/navbar.php');
      include_once('includes/login.php');
    ?>
    <?php if($_SESSION['user']['idRango'] < 3 && $_SESSION['user']['idRango'] != ""){?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header bg-info">
            <h4 class="mb-0 text-white">Añadir servicio</h4>
          </div>
          <form>
            <div class="form-body">
              <div id="main-añadir-servicio" class="card-body">
                <div class="row pt-3">
                  <div class="col-md-2">
                    <div class="form-group">
                      <label for="dni" class="control-label">DNI</label>
                      <input type="text" id="dni" class="form-control form-control-danger" placeholder="DNI">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label">Nombre</label>
                      <input type="text" id="nombre" class="form-control" placeholder="Nombre">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group has-danger">
                      <label class="control-label">Apellidos</label>
                      <input type="text" id="lastName" class="form-control" placeholder="Apellido">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group has-danger">
                      <label class="control-label">Email</label>
                      <input type="text" id="email" class="form-control" placeholder="Email">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group has-danger">
                      <label class="control-label">Teléfono</label>
                      <input type="text" id="telefono" class="form-control" placeholder="Teléfono">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Dirección</label>
                      <input type="text" id="direccion" class="form-control" placeholder="Dirección">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Población</label>
                      <input type="text" id="poblacion" class="form-control form-control-danger" placeholder="Población" >
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div id="provincia">
                      <label for="final-provincia" class="control-label">Provincia</label>
                      <input id="final-provincia" class="typeahead form-control" disabled type="text" placeholder="Provincia" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2">
                    <div class="form-group has-success">
                      <label for="localizacion" class="control-label">Localización</label>
                      <select id="localizacion" disabled class="form-control custom-select text-capitalize" required>
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group has-success">
                      <label class="control-label">Creado por</label>
                      <select id="creado-por" disabled class="form-control custom-select">
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group has-success">
                      <label for="tipo-servicio" class="control-label">Tipo de servicio</label>
                      <select id="tipo-servicio" disabled class="form-control custom-select" required>
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group has-success">
                      <label for="tecnico" class="control-label">Técnico</label>
                      <select id="tecnico" class="form-control custom-select" required>
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group has-success">
                      <label class="control-label">Pasado por</label>
                      <select id="pasado-por" disabled class="form-control custom-select">
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label>Información del servicio</label>
                    <textarea id="informacion-servicio" class="form-control" rows="5"></textarea>
                  </div>
                  <div class="col-md-6">
                    <label>Planificación Agenda</label>
                    <textarea id="planificacion-agenda" class="form-control" rows="5"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col text-center p-3">
                    <button class="btn btn btn-default" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Facturación</button>
                  </div>
                </div>
                <div class="collapse" id="collapseExample">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">Dirección facturación</label>
                        <input type="text" id="direccion_facturacion" class="form-control" placeholder="Dirección">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="control-label">Población facturación</label>
                        <input type="text" id="poblacion_facturacion" class="form-control form-control-danger" placeholder="Población">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group has-success">
                        <label class="control-label">Provincia facturación</label>
                        <select id="provincia_facturacion" class="form-control custom-select">
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">CP facturación</label>
                        <input type="text" id="CP_facturacion" class="form-control" placeholder="CP">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="control-label">Nº Albarán</label>
                        <input type="text" id="albaran" class="form-control" placeholder="Nº Albarán">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="control-label">Ingresos</label>
                        <input type="text" id="ingresos" class="form-control" placeholder="Ingresos">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="control-label">IVA</label>
                        <input type="text" id="iva" class="form-control" placeholder="IVA">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="control-label">Material</label>
                        <input type="text" id="material" class="form-control" placeholder="Material">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">Ingresos técnico</label>
                        <input type="text" id="ingresos-tecnico" class="form-control" placeholder="Ingresos técnico">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">Material técnico</label>
                        <input type="text" id="material-tecnico" class="form-control" placeholder="Material técnico">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">Guardia</label>
                        <input type="text" id="guardia" class="form-control" placeholder="Guardia">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">Gasolina</label>
                        <input type="text" id="gasolina" class="form-control" placeholder="Gasolina">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">Otros</label>
                        <input type="text" id="otros" class="form-control" placeholder="Otros">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">Pasado por</label>
                        <input type="text" id="pasado-por-ingresos" class="form-control" placeholder="Pasado por">
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-center">
                    <div class="col-md-3">
                      <div class="form-group has-success">
                        <label class="control-label">Facturado</label>
                        <select id="facturado" class="form-control custom-select" value="">
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <div class="form-group">
                        <label class="control-label">Revisión técnico</label>
                        <input type="checkbox" id="revision-tecnico" class="form-control checkbox mx-auto checkbox-special-size">
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <div class="form-group">
                        <label class="control-label">Liquidado</label>
                        <input type="checkbox" id="liquidado" class="form-control checkbox mx-auto checkbox-special-size">
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <div class="form-group">
                        <label class="control-label">Comprobado</label>
                        <input type="checkbox" id="comprobado" class="form-control checkbox mx-auto checkbox-special-size">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 px-1 datepicker-container">
                    <div class="form-group">
                      <label>Fecha entrada</label>
                      <input type="text"id="fecha-entrada" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-4 px-1 datepicker-container">
                    <div class="form-group">
                      <label>Fecha Agenda</label>
                      <input id="fecha-agenda" type="text" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-4 px-1 datepicker-container">
                    <div class="form-group">
                      <label>Fecha Finalización</label>
                      <input id="fecha-finalizacion" type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="card-body">
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                <button type="button" class="btn btn-dark"> <i class="far fa-times-circle"></i> Cancelar</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php
      } else if($_SESSION['user']['idRango'] >= 3 ){
        include_once('includes/errors/error-403.html');
      }
    ?>
    </div>
    <?php
      include_once('includes/footer.php');
    ?>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <script src='assets/js/common.js'></script>
  <script src='assets/js/js.js'></script>
  <script src='assets/js/jSignature.js'></script>
  <script>
    $(document).ready(function() {
      proyectoAqualimp.initEvents();
      <?php if($_SESSION['user']['idRango'] != null){?>
      $('.username').data("userId", "<?php echo $_SESSION['user']['idEmpleado']; ?>");
      <?php if($_SESSION['user']['idRango'] < 3){ ?>
      proyectoAqualimp.cargarItemsServicios();
      <?php } ?>
      <?php } ?>
      $('.datepicker-container input').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        language: "es",
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true
      });
    });

  </script>
  <script src="assets/datepicker/js/bootstrap-datepicker.min.js"></script>
  <script src="assets/datepicker/js/bootstrap-datepicker.js" charset="UTF-8"></script>
  <script src="assets/datepicker/locales/bootstrap-datepicker.es.min.js" charset="UTF-8"></script>
  <script src="assets/js/bloodhound.min.js"></script>
  <script src="assets/js/typeahead.jquery.min.js" charset="UTF-8"></script>

</html>
