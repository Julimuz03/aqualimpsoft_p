<?php
  if(session_status() != PHP_SESSION_ACTIVE){
    session_start();
  }

  if(isset($_COOKIE['SessionCookie'])){
    $_SESSION['user'] = json_decode($_COOKIE['SessionCookie'], true);
    setcookie("SessionCookie", "", time() - 3600, "/");
  }

  if(isset($_COOKIE['SessionDestroy'])){
    session_destroy();
    setcookie('SessionDestroy', "", time()-3600, "/");
    session_start();
  }

  if( !isset( $_SESSION['user']['idRango'] ) ){
    $_SESSION['user'] = null;
    $_SESSION['user']['idRango'] = null;
  }

?>
