﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AqualimpSoft.Controllers
{
    public class ServiciosController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage Prueba(string user, string password)
        {

            object Result = new object();

            List<object> Results = new List<object>();

            Results.Add(user);
            Results.Add(password);


            string JsonResult = JsonConvert.SerializeObject(Results, Formatting.None);

            return Request.CreateResponse(HttpStatusCode.OK,JsonResult);
        }

    }
}
