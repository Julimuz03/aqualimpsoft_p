﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace AqualimpSoft.Controllers
{
    public class ServiceController : ApiController
    {
        // GET: api/Service
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Service/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Service
        //[HttpPost("Pruebas")]
        public string Post([FromBody]string user, string password)
        {
            object Result = new object();

            List<object> Results = new List<object>();

            Results.Add(user);
            Results.Add(password);


            string JsonResult = JsonConvert.SerializeObject(Results, Formatting.None);

            return JsonResult;
        }

        // POST: /api/items/PostSomething
       
        //public string Post([FromBody]string user, string password)
        //{
        //    object Result = new object();

        //    List<object> Results = new List<object>();

        //    Results.Add(user);
        //    Results.Add(password);


        //    string JsonResult = JsonConvert.SerializeObject(Results, Formatting.None);

        //    return JsonResult;
        //}

        // PUT: api/Service/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Service/5
        public void Delete(int id)
        {
        }
    }
}
