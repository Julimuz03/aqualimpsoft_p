<html>
<?php
  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );

  include_once('session/sessions.php');
?>
  <head>
    <?php include_once('includes/head.php');?>
    <link rel='stylesheet' type='text/css' href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel='stylesheet' type='text/css' href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel='stylesheet' type='text/css' href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css">

  </head>
  <body>
    <div id="main">
    <?php
      include_once('includes/navbar.php');
      include_once('includes/login.php');

      if($_SESSION['user']['idRango'] < 3 && $_SESSION['user']['idRango'] != null){
        include_once('includes/card-stats.php');
        include_once('includes/card-buttons.php');
      }

      if($_SESSION['user']['idRango'] == 3){
        include_once('includes/servicios-tecnico.php');
      }
    ?>
    </div>
    <?php
      include_once('includes/footer.php');
    ?>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <script src='assets/js/common.js'></script>
  <script src='assets/js/js.js'></script>
  <script src='//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
  <script src='//cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js'></script>
  <script src='//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js'></script>
  <script src='assets/js/jSignature.js'></script>
  <script src='assets/js/modernizr-custom.js'></script>
  <script>
    $(document).ready(function() {
      proyectoAqualimp.initEvents();
      <?php if($_SESSION['user']['idRango'] != null){?>
      $('.username').data("userId", "<?php echo $_SESSION['user']['idEmpleado']; ?>");
      <?php if($_SESSION['user']['idRango'] == 3){ ?>
      proyectoAqualimp.cargarServicios();
      <?php } ?>
      <?php } ?>
    });
  </script>
</html>
