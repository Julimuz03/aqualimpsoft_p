﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace AqualimpSoft
{
    /// <summary>
    /// Descripción breve de WsTest
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class WsTest : System.Web.Services.WebService
    {

        [WebMethod]
        public string Prueba(string user, string password)
        {
            object Result = new object();

            List<object> Results = new List<object>();

            Results.Add(user);
            Results.Add(password);


            string JsonResult = JsonConvert.SerializeObject(new
            {
                USUARIO = user,
                PASSWORD = password,

            }, Formatting.None);

            return JsonResult;

        }
    }
}
