<html>
<?php
  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );

  include_once('session/sessions.php');
?>
  <head>
    <link rel="shortcut icon" href="assets/images/icono-aqua.png" type="image/x-icon" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel='stylesheet' type='text/css' href='assets/css/nav.css'>
    <link rel='stylesheet' type='text/css' href='assets/css/login.css'>
    <link href="assets/css/style.min.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css' href="assets/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <?php
      include_once('includes/navbar.php');
    ?>
    <div id="main-content">
      <div class="col-sm-12 col-md-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Búsqueda de empleado</h4>
            <form class="m-t-20">
              <div class="input-group">
                <input id="busqueda-usuario" type="text" class="form-control" placeholder="Usuario">
                <div class="input-group-append">
                  <button class="btn btn-outline-secondary buscar-usuario" type="button">Buscar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <?php
      include_once('includes/footer.php');
    ?>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <script src='assets/js/common.js'></script>
  <script src='assets/js/js.js'></script>
  <script src='//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>

  <script>
    $(document).ready(function() {
      proyectoAqualimp.initEvents();
    });
  </script>
</html>
