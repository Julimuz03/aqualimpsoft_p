﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using Security;
using Security.Negocio;

namespace AqualimpSoft.Login
{
    /// <summary>
    /// Descripción breve de Login
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Login : System.Web.Services.WebService
    {

        [WebMethod]
        public string crearUsuario(string IdEmpleado, string usuario, string pass, string IdRango, string UsuarioLog)
        {
            clsSecurity objConsulta = new clsSecurity();


            DataTable dtResultado = objConsulta.CrearUsuario(IdEmpleado, usuario, pass, int.Parse(IdRango), UsuarioLog);

            if (dtResultado.Rows.Count > 0)
            {
                //int
                string JsonResult = JsonConvert.SerializeObject(dtResultado);
                return JsonResult;
            }
            else
            {
                return string.Empty;
            }

        }


        [WebMethod]
        public string validarUsuario(string Usuario, string Pass)
        {
            try
            {
                clsSecurity objConsulta = new clsSecurity();

                var objIsValidUser = objConsulta.ValidarUsuario(Usuario, Pass);

                if (objIsValidUser.Bool)
                {
                    clsNegocio objConsultas = new clsNegocio();

                    string DatosUsuario;

                    DataTable dtResultado = objConsultas.LoginUsuarios(Usuario);

                    if (dtResultado.Rows.Count > 0)
                    {
                        DatosUsuario = JsonConvert.SerializeObject(dtResultado);
                    }
                    else
                    {
                        return "-1";
                    }

                    return DatosUsuario;
                }
                else
                {
                    string DatosUsuario = JsonConvert.SerializeObject(objIsValidUser.Tabla);

                    return DatosUsuario;
                }
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(e.Message.ToString());
            }

        }
    }
}
