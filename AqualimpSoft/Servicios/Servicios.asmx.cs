﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using Security.Negocio;

namespace AqualimpSoft.Servicios
{
    /// <summary>
    /// Descripción breve de Servicios
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Servicios : System.Web.Services.WebService
    {
        //*//
        [WebMethod]
        public string CrearServicios()
        {
            clsNegocio objConsulta = new clsNegocio();

            //recibir los parametros de crear servicio.

            return String.Empty;
        }


        [WebMethod]
        public string getInfoPpalServicios(string idEmpleado)
        {
            clsNegocio objConsulta = new clsNegocio();

            DataSet dtResultado = objConsulta.getInfoPpalServicios();

            if (dtResultado.Tables.Count > 0)
            {

                var JsonResultLocalizacion = JsonConvert.DeserializeObject(dtResultado.Tables[0].Rows[0][0].ToString());
                var JsonCreadoX = JsonConvert.DeserializeObject(dtResultado.Tables[1].Rows[0][0].ToString());
                var JsonTipoServicio = JsonConvert.DeserializeObject(dtResultado.Tables[2].Rows[0][0].ToString());
                var JsonPasadoX = JsonConvert.DeserializeObject(dtResultado.Tables[3].Rows[0][0].ToString());
                // var JsonProvincias = JsonConvert.DeserializeObject(dtResultado.Tables[4].ToString());
                var JsonProvincias = dtResultado.Tables[4];


                object Result = new object();

                List<object> Results = new List<object>();

                Results.Add(JsonResultLocalizacion);
                Results.Add(JsonCreadoX);
                Results.Add(JsonTipoServicio);
                Results.Add(JsonPasadoX);
                Results.Add(JsonProvincias);

                string JsonResult = JsonConvert.SerializeObject(new
                {
                    LOCALIZACION = JsonResultLocalizacion,
                    USUARIO_CREACION = JsonCreadoX,
                    TIPO_SERVICIO = JsonTipoServicio,
                    PASADOX = JsonPasadoX,
                    PROVINCIAS = JsonProvincias
                }, Formatting.None);

                return JsonResult;
            }
            else
            {
                return "-1";
            }

        }

        [WebMethod]
        public string getInfoPpalTecnicos(string idProvincia, string idTipoServicio)
        {
            clsNegocio objConsulta = new clsNegocio();

            DataSet dtResultado = objConsulta.getPoblaciones(int.Parse(idProvincia),int.Parse(idTipoServicio));

            if (dtResultado.Tables.Count > 0)
            {

                var JsonPueblos = dtResultado.Tables[0];
                var JsonTecnicos = dtResultado.Tables[1];

                object Result = new object();

                List<object> Results = new List<object>();

                Results.Add(JsonPueblos);
                Results.Add(JsonTecnicos);


                string JsonResult = JsonConvert.SerializeObject(new { POBLACIONES = JsonPueblos, TECNICOS = JsonTecnicos }, Formatting.None);

                return JsonResult;
            }
            else
            {
                return "-1";
            }
            //crear los otros metodos que traigan la informacion, consultar info
        }
    }
}
