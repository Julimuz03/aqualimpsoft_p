proyectoAqualimp = {
  initEvents: function() {
    console.log("Cargando eventos");
    $('.login').on('submit', function(e) {
      /*****************************************************************************************************/
      /* Evento: submit en .login                                                                          */
      /* Evento que se activa cuando envías el formulario de Login, éste mandará los datos en un JSON al C#*/
      /* Si los datos no son correctos se creará una modal de error                                        */
      /*****************************************************************************************************/

      e.preventDefault();
      var $this = $(this),
      $state = $this.find('button > .state');
      $this.addClass('loading');
      $state.html("Autentificando");

      let datos = {
        'Usuario': $('#user').val(),
        'Pass': $('#password').val()
      }

      proyectoAqualimp.peticionAJAX('/Login/login.asmx/validarUsuario', datos)
        .then(data => {
          console.log(data)
          if (data.d.length > 3) {
            data = $.parseJSON(data.d)[0]
            if (data.codigo == "1") {

              $this.addClass('ok');
              $state.html("¡Bienvenido!");
              createCookie('SessionCookie', JSON.stringify(data), 1)
              console.log(data)
              setTimeout(function() {

                $state.html("Entrando...");
                $this.removeClass('ok loading');
                document.location.reload()

              }, 2000);

            }

          } else if (data.d == "-1" || data.d == "-2") {

            $state.html("Conectar");
            $this.removeClass('ok loading');
            createModal("Error", "Usuario y contraseña incorrectos")

          }
        })
    });
    $('.logout').on('click', function(e) {
      e.preventDefault()
      createCookie('SessionDestroy', true, 1)
      document.location.reload()
    })
    $('body').on('hidden.bs.modal', function(e) {
      /*****************************************************************************************************/
      /* evento; hidden en una modal de bootstrap                                                          */
      /* Este borrará la modal en el caso que esta se esconda                                              */
      /*****************************************************************************************************/
      working = false;
      $('#notificacion').modal('hide')
      $('#notificacion').remove()

    })
    $('#tecnicos').on('click', function(e) {
      document.location.href = "tecnicos.php"
    })
    $('#usuarios').on('click', function(e) {
        document.location.href = "usuarios.php"
      }),
      $('#añadir-servicio').on('click', function(e) {
        document.location.href = "añadir-servicio.php"
      }),
      $('.buscar-usuario').on('click', function(e) {
        let datos = {
          'idEmpleado': $('#busqueda-usuario').val()
        }
        proyectoAqualimp.peticionAJAX('Principal.aspx/validarNomEmpleado', datos)
          .then(data => {
            data = $.parseJSON(data.d)
            console.log(data)
            if (data != "-1") {
              datosEmpleado = data[0].DATOS[0]
              listaDeRangos = data[1].RANGOS
              let empleado = '<div id="empleado" class="col-sm-12 col-md-12" style="display:none"><div class="card"><div class="card-body"><h4 class="card-title">Datos de empleado</h4><div class="row"><div class="col-sm-6 col-md-6"><div class="form-group"><label for="inputcom" class="control-label col-form-label">Nombre</label><input readonly value="' + datosEmpleado.nombre + '" type="text" class="form-control" id="nombre"></div></div><div class="col-sm-6 col-md-6"><div class="form-group"><label for="inputcom" class="control-label col-form-label">Cargo</label><input readonly type="text" value="' + datosEmpleado.cargo + '" class="form-control" id="cargo" placeholder="cargo"></div></div></div></div></div></div>'
              let rangos = '<div id="rangos" class="col-sm-12 col-md-12" style="display:none"><form class="needs-validation"><div class="card"><div class="card-body"><h4 class="card-title">Registro de usuario</h4> <div class="row"><div class="col-sm-6 col-md-4"><div class="form-group"><label for="inputcom" class="control-label col-form-label">Usuario</label><input type="text" class="form-control" id="user" placeholder="Nombre de usuario" required></div></div><div class="col-sm-6 col-md-4"><div class="form-group"><label for="inputcom" class="control-label col-form-label">Contraseña</label><input type="password" class="form-control" id="password" placeholder="Contraseña" required></div></div><div class="col-sm-6 col-md-4"><div class="form-group"><label class="control-label col-form-label">Rangos</label><select id="lista-de-rangos" class="form-control" required><option value="null">Escoge un rango</option>'

              Object.keys(listaDeRangos).forEach(function(item) {
                rangos += '<option value="' + listaDeRangos[item].Id_Rango + '">' + listaDeRangos[item].Desc_Rango + '</option>'
              })
              rangos += '</select></div></div></div><button id="guardar-usuario" type="submit" class="float-sm-right btn btn-primary aqua-color"><i class="fas fa-check"></i> Guardar</button></div></div></form></div>'

              $('#main-content').append(empleado)
              $('#main-content').append(rangos)
              $('#empleado').fadeIn()
              $('#rangos').fadeIn()
            }
          })
      })
    $('body').on('click', '#guardar-usuario', function(e) {
      var check = true;
      $('*[required]').each(function() {
        if ($(this).val().trim().length < 1 || $(this).val() == "null") {
          check = false;
        }
      });
      if (check) {
        e.preventDefault()
        let datos = {
          'IdEmpleado': $('#busqueda-usuario').val(),
          'usuario': $('#user').val(),
          'pass': $('#password').val(),
          'IdRango': $('#lista-de-rangos').val(),
          'UsuarioLog': $('.username').attr('value')
        }
        proyectoAqualimp.peticionAJAX("Principal.aspx/crearUsuario", datos)
          .then(data => {
            data = $.parseJSON(data.d)[0]
            if (data.Codigo == "1") {
              createModal('Datos enviados correctamente', data.mensaje)
            } else if (data.codigo == "-1")
              createModal('Error', data.mensaje)
          })
      }
    })
    $('body').on('click', '.terminado', function(e) {
      $(this).parent().find('.opciones').fadeIn()
    })
    $('body').on('click', '.activateFile', function(e) {
      $(this).parent().find('.hideFile').click()
    })
    $('body').on('click', '.firma-cliente', function(e) {
      var actualItem = $(this)
      createModalButtons('Firma del cliente', '<div id="signatureparent"><div id="signature"></div></div>', '<input type="button" class="btn btn-primary save-firma" value="Guardar"><input type="button" class="btn btn-primary reset-firma" value="Reiniciar">')
      $('#signature').jSignature({
        color: "black",
        lineWidth: 2,
        width: "350px",
        height: 200
      });
      $('body').on('click', '.save-firma', function(e) {
        actualItem.parent().find('.enviar-servicio').data('firmaData', $('.jSignature')[0].toDataURL('image/png'))
        actualItem.parent().find('.enviar-servicio').attr('firma', true)
        $('#notificacion').modal('hide')
      })
    })
    $('body').on('click', '.reset-firma', function(e) {
      $('#signature').jSignature('reset')
    })
    $('body').on('change', '.hideFile', async function(e) {
      var fileName = e.target.files[0].name;
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      var actualItem = $(this).parent().find('.enviar-servicio')
      actualItem.attr('canvas', 'loading')
      reader.onload = event => {
        var img = new Image();
        img.src = event.target.result;
        img.onload = () => {
            var elem = document.createElement('canvas');
            elem.width = img.width;
            elem.height = img.height;
            var ctx = elem.getContext('2d');
            ctx.drawImage(img, 0, 0, img.width, img.height);
            ctx.canvas.toBlob((blob) => {
              var file = new File([blob], fileName, {
                type: 'image/jpeg',
                lastModified: Date.now()
              });

            }, 'image/jpeg', 0.9);
            actualItem.data('canvasData', ctx.canvas.toDataURL('image/jpeg', 0.1))
            actualItem.attr('canvas', true)
          },
          reader.onerror = error => console.log(error);
      };
    })
    $('body').on('click', '.enviar-servicio', function(e) {
        if ($(this).attr('canvas') == "true" && $(this).attr('firma') == "true") {
          let datos = {
            'usuarioLog': $('.username').attr('value'),
            'albaranArchivo': $(this).data('canvasData'),
            'idServicio': $(this).attr('id-servicio'),
            'firmaCliente': $(this).data('firmaData'),
            'notaTecnico': $(this).parent().parent().find('textarea').val()
          }

          proyectoAqualimp.peticionAJAX('Principal.aspx/setServicioRealizado', datos)
            .then(data => {
              console.log(data)
            })
            .catch(error => {
              console.log(error)
            })
        } else {
          let firma = createStateElement($(this).attr('firma'), "firma", "femenino");
          let canvas = createStateElement($(this).attr('canvas'), "albarán", "masculino");
          console.log(firma, canvas)
          createModal("Todavía no se han procesado todos los datos", firma + canvas, )
        }
      }),
      $('body').on('focusout', '#tipo-servicio', function(e) {

        if ($(this).val() != undefined) {
          let idProvincia = Number($('#provincia>.twitter-typeahead>.tt-input').attr('value')) || "";
          let idTipoServicio = Number($('#tipo-servicio').val()) || "";

          let datos = {
            'idProvincia': idProvincia,
            'idTipoServicio': idTipoServicio
          }

          proyectoAqualimp.peticionAJAX('Principal.aspx/getInfoPpalServicios', datos)
            .then(datos => {
              console.log(datos)
            })
        }
      })
  },
  cargarServicios: function() {
    let datos = {
      'idEmpleado': $('.username').data('userId')
    }

    proyectoAqualimp.peticionAJAX('Principal.aspx/getServiciosTecnico', datos)
      .then(data => {
        console.log(data)
        data = $.parseJSON(data.d);

        servicios = data.SERVICIOS
        serviciosLiquidar = data.SERVICIOS_X_LIQUIDAR
        serviciosRevision = data.SERVICIOS_X_REVISION
        console.log(data)

        if (servicios[0].respuesta == undefined) {
          proyectoAqualimp.appendObjects("tabla-pendientes", servicios)
          $('#collapsePendientesLiquidar').collapse()
        } else {
          proyectoAqualimp.appendNoObjects("tabla-pendientes")
          $('#collapsePendientes').collapse()
        }

        if (serviciosLiquidar[0].respuesta == undefined) {
          proyectoAqualimp.appendObjects("tabla-pendientes-liquidar", serviciosLiquidar)
        } else {
          proyectoAqualimp.appendNoObjects("tabla-pendientes-liquidar")
        }

        if (serviciosRevision[0].respuesta == undefined) {
          proyectoAqualimp.appendObjects("tabla-pendientes-revision", serviciosRevision)
        } else {
          proyectoAqualimp.appendNoObjects("tabla-pendientes-revision")
        }
        $('#collapsePendientesRevision').collapse()
      })
      .catch(error => {
        console.log(error)
      })
  },
  cargarItemsServicios: function() {
    let datos = {
      'idEmpleado': $('.username').data('userId')
    }
    proyectoAqualimp.peticionAJAX('Principal.aspx/getInfoPpalServicios', datos)
      .then(data => {
        data = $.parseJSON(data.d);
        console.log(data)

        proyectoAqualimp.prepareSelect('localizacion', data.LOCALIZACION)
        proyectoAqualimp.prepareSelect('pasado-por', data.PASADOX)
        proyectoAqualimp.prepareSelect('creado-por', data.USUARIO_CREACION)
        proyectoAqualimp.prepareSelect('tipo-servicio', data.TIPO_SERVICIO)
        proyectoAqualimp.prepareBlodhound('provincia', data.PROVINCIAS)

      })
      .catch(error => {
        console.log(error)
      })
  },
  peticionAJAX: function(url, datos) {
    console.log(datos)
    /*****************************************************************************************************/
    /* función: peticionAJAX()                                                                           */
    /* Esta función lanza una petición AJAX al servidor dada una URL y unos datos, la función devuelve   */
    /* una promesa para tratar los datos una vez acabe                                                   */
    /*****************************************************************************************************/
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(datos),
        processData: false,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(data) {
          resolve(data)
        },
        error: function(e) {
          reject(e)
        }
      })
    })
  },
  appendObjects: function(id, object) {
    $('#' + id).append('<table id="js-' + id + '" class="display nowrap"><thead><th>ID</th><th>Tel.</th><th>Poblacion</th><th>Dirección</th><th>Provincia</th><th>Fecha</th><th>Nombre</th><th>Estado</th><th>Usuario creación</th><th>Notas</th><th>Opciones</th></thead><tbody class="tbody-db"></tbody></table>')
    Object.keys(object).forEach(function(item) {
      let actualItem = object[item]
      let toAppend = "";
      if (actualItem.notas !== undefined) {
        actualItem.notas.forEach(function(item) {
          toAppend += "<div class='notas'>" + item.fechaNota + " " + item.nota + "</div>"
        })
      }
      let opciones = ""

      tr = '<tr><td>' + actualItem.idServicio + '</td><td>' + actualItem.telefonoServicio + '</td><td>' + actualItem.poblacion + '</td><td>' + actualItem.direccionServicio + '</td><td>' + actualItem.provincia + '</td><td>' + actualItem.fechaCreacion + '</td><td>' + actualItem.nombre + '</td><td>' + actualItem.estado + '</td><td>' + actualItem.usuarioCreacion + '</td><td>' + toAppend + '</td><td>'
      if (id == "tabla-pendientes") {
        tr += '<div><input class="terminado" type="button" value="¿Has terminado el servicio?" class="btn btn-primary"><div class="opciones"><label>Notas del servicio</label><textarea  class="md-textarea form-control textareaNotaTecnico" rows="3"></textarea><div class="align-buttons"><input class="hideFile" type="file" accept="image/*;capture=camera"><input value="Firma" type="button" class="btn btn-primary firma-cliente"><input value="Foto Albarán" class="activateFile btn btn-primary" type="button" class="btn btn-primary"><input class="value-firma-cliente hide" class="value"><input value="Enviar" class="btn btn-success enviar-servicio" firma="false" canvas="false" id-servicio="' + actualItem.idServicio + '" type="button"></div></div></div>'
      }
      tr += "</td></tr>"
      $('#js-' + id).append(tr)


    })
    $('#js-' + id).DataTable(TABLE_ES);
  },
  appendNoObjects: function(id) {
    $('#' + id).append('<p>No hay datos que mostrar</p>')
  },
  prepareBlodhound: function(id, data) {
    var datos = [];
    data.forEach(function(item) {
      datos.push(item.descripcion)
    });
    var datos2 = datos;

    datos = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: datos
    });
    $('#' + id + ' .typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    }, {
      name: 'datos',
      source: datos
    });
    $('#' + id + '>.twitter-typeahead>.tt-input').attr('disabled', false)

    $('#' + id + '>.twitter-typeahead>.tt-input').focusout(function(e) {
      var itemIndex = datos2.indexOf($(this).val())
      if (itemIndex >= 0) {
        $(this).attr('value', itemIndex)
        let idProvincia = Number(itemIndex) || "";
        let idTipoServicio = Number($('#tipo-servicio').val()) || "";

        let datos = {
          'idProvincia': idProvincia,
          'idTipoServicio': idTipoServicio
        }

        proyectoAqualimp.peticionAJAX('Principal.aspx/getInfoPpalServicios', datos)
          .then(datos => {
            console.log(datos)
          })
      }
    })
  },
  prepareSelect: function(id, data) {
    let dataToAppend;
    data.forEach(function(item) {
      dataToAppend += `<option value='${item.id}'>${item.descripcion}</option>`
    })
    $('#' + id).append(dataToAppend)
    $('#' + id).attr('disabled', false)
  }
};
