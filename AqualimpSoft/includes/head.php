<link rel="shortcut icon" href="assets/images/icono-aqua.png" type="image/x-icon" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel='stylesheet' type='text/css' href='assets/css/nav.css'>
<link rel='stylesheet' type='text/css' href='assets/css/login.css'>
<link href="assets/css/style.min.css" rel="stylesheet">
<link rel='stylesheet' type='text/css' href="assets/css/main.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">
