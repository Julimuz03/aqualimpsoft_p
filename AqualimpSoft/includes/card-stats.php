<div class="card-group">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h3>203</h3>
          <h6 class="card-subtitle">Total servicios</h6>
        </div>
        <div class="col-12">
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h3>106</h3>
          <h6 class="card-subtitle">Servicios Cobrados</h6>
        </div>
        <div class="col-12">
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h3>40</h3>
          <h6 class="card-subtitle">Servicios nulos</h6>
        </div>
        <div class="col-12">
          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h3>56</h3>
          <h6 class="card-subtitle">Nulos</h6>
        </div>
        <div class="col-12">
          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h3>26</h3>
          <h6 class="card-subtitle">Presupuestos</h6>
        </div>
        <div class="col-12">
          <div class="progress">
            <div class="progress-bar bg-inverse" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <h3>52</h3>
          <h6 class="card-subtitle">Pendientes</h6>
        </div>
        <div class="col-12">
          <div class="progress">
            <div class="progress-bar bg-inverse" role="progressbar" style="width: 52%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
