
<div class="button-container">
  <div class="card-group">
    <div id="añadir-servicio" class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Añadir servicio</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-clipboard"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div id="lista-de-servicios" class="card-body text-center">
        <h4 class="text-center text-info">Lista de servicios</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  far fa-list-alt"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Informe de servicios</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6 ti-bar-chart"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-group">
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Añadir cliente</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-user"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Lista de clientes</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6 ti-agenda"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Correos de clientes</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-email"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-group">
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Contabilidad</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-clipboard"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Liquidaciones</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-close"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Agenda</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6 ti-calendar"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-group">
    <div id="tecnicos" class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Técnicos</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-briefcase"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="usuarios" class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Usuarios</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-clipboard"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Colaboradores</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-clipboard"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-group">
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Albaranes</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-briefcase"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body text-center">
        <h4 class="text-center text-info">Facturas</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6  ti-clipboard"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-group">
    <div class="card">
      <div class="card-body text-center" id="adwords">
        <h4 class="text-center text-info">Adwords</h4>
        <div class="row p-t-10 p-b-10">
          <div class="col text-center align-self-center">
            <div class="css-bar m-b-0 css-bar-primary css-bar-20">
              <i class="display-6 ti-briefcase"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
