<nav class="navbar navbar-expand-lg navbar-light nav-color">
  <a class="navbar-brand" href="#">Proyecto liquidados Aqualimp</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="main-nav">

    <?php if($_SESSION['user']['idRango'] > 0) { ?>
    <ul class="navbar-nav mt-2 mt-lg-0 ml-auto">
      <li class="nav-item active">
        <a class="nav-link text-white" href="index.php"><span class="home">Inicio</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white"><span value="<?php echo $_SESSION['user']['usuario']; ?>" class="username"><?php echo $_SESSION['user']['nombre']; ?></span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white logout" href="#"><span>Desconectar</span></a>
      </li>
    </ul>
    <?php } else if( $_SESSION['user']['idRango'] == 0 || $_SESSION['user']['idRango'] == null){?>
    <ul class="navbar-nav mt-2 mt-lg-0 ml-auto">
      <form class="form-inline my-2 my-lg-0">
        <!-- <button id="signin" class="btn btn-outline-primary nav-log-transition my-2 my-sm-0 bg-white"><span class="lang-login">Conéctate</span></button> -->
      </form>
    </ul>
    <?php } ?>
  </div>
</nav>
