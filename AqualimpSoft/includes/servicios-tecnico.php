
  <div id="accordion">
    <div class="card">
      <div class="card m-b-5">
        <div class="card-header" id="headingPendientes">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapsePendientes" aria-expanded="false" aria-controls="collapsePendientes">
              Servicios pendientes
            </button>
          </h5>
        </div>
        <div id="collapsePendientes" class="collapse show" aria-labelledby="headingPendientes" data-parent="#accordion">
          <div class="card-body">
            <div class="" id="tabla-pendientes"> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card m-b-5">
        <div class="card-header" id="headingPendientesRevision">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapsePendientesRevision" aria-expanded="false" aria-controls="collapsePendientesRevision">
              Servicios pendientes de revisión
            </button>
          </h5>
        </div>
        <div id="collapsePendientesRevision" class="collapse show" aria-labelledby="headingPendientesRevision" data-parent="#accordion">
          <div class="card-body">
            <div class="content col-md-12" id="tabla-pendientes-revision"> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card m-b-5">
        <div class="card-header" id="headingPendientesLiquidar">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapsePendientesLiquidar" aria-expanded="false" aria-controls="collapsePendientesLiquidar">
              Servicios por liquidar
            </button>
          </h5>
        </div>
        <div id="collapsePendientesLiquidar" class="collapse show" aria-labelledby="headingPendientesLiquidar" data-parent="#accordion">
          <div class="card-body">
            <div class="content col-md-12" id="tabla-pendientes-liquidar"> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
