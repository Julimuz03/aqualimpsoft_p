<?php if( $_SESSION['user']['idRango'] == 0 || $_SESSION['user']['idRango'] == null) {?>
<div class="p-5 login-div">
  <form class="login m-auto">
    <p class="title">Login</p>
    <input type="text" id="user" placeholder="Usuario" autofocus/>
    <i class="fa fa-user"></i><input id="password" type="password" placeholder="Contraseña" />
    <i class="fa fa-key"></i>
    <a href="#">¿Has olvidado la contraseña?</a>
    <button><i class="spinner"></i><span class="state">Conectar</span></button>
  </form>
</div>
<?php }?>
